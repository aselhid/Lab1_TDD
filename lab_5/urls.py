from django.conf.urls import url
from .views import index, add_todo

#url for app, add your URL Configuration

urlpatterns = [
    #TODO Implement this
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
]
