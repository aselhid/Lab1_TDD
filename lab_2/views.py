from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac mi pharetra, aliquet urna nec, dictum quam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer laoreet, tortor vitae ornare bibendum, purus augue pharetra dui, ut pellentesque felis odio vel eros. Etiam dui dolor, aliquam iaculis lorem in, commodo suscipit turpis. Vestibulum tristique at nisl sed hendrerit. Praesent in velit odio. Fusce sed nibh urna. Praesent ac libero ut nibh vulputate posuere eu vitae nisl.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)