// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '849793435194770',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  FB.getLoginStatus((response) => {
    const loggedIn = response.status === 'connected';

    render(loggedIn);
  });
};

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

const render = loginFlag => {
  if (loginFlag) {
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<div class="profile">' +
          '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
          '<div class="profile-header">' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
        '</div>' +
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
        '<button class="logout" onclick="facebookLogout()">Logout</button>'
      );

      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="feed" id="post_' + value.id + '">' +
                '<h1>' + value.message + '</h1>' +
                '<h2>' + value.story + '</h2>' +
                '<button onclick="deleteStatus(\'' + value.id + '\')">Delete</button>' +
              '</div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="feed" id="post_' + value.id + '">' +
                '<h1>' + value.message + '</h1>' +
                '<button onclick="deleteStatus(\'' + value.id + '\')">Delete</button>' +
              '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="feed" id="post_' + value.id + '">' +
                '<h2>' + value.story + '</h2>' +
                '<button onclick="deleteStatus(\'' + value.id + '\')">Delete</button>' +
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
  }
};

const facebookLogin = () => {
  FB.login((response) => {
     console.log(response);
     location.reload();
   }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'});
};

const facebookLogout = () => {
  FB.getLoginStatus((response) => {
    if (response.status === 'connected') {
      FB.logout(() => {
        location.reload();
      });
    }
   });
};

const getUserData = (render) => {
  FB.getLoginStatus((response) => {
    if (response.status === 'connected') {
      FB.api('/me?fields=id,name,cover,picture,about,email,gender', 'GET', (response) => {
        render(response);
      });
    }
  });
};

const getUserFeed = (render) => {
  FB.getLoginStatus((response) => {
    if (response.status === 'connected') {
      FB.api('/me?fields=feed', 'GET', (response) => {
        console.log(response.feed);
        render(response.feed);
      });
    }
  });
};

const postFeed = () => {
  const message = $("#postInput").val();
  FB.api('/me/feed', 'POST', {message:message}, () => {
    $("#postInput").val('');
  });
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
};

const deleteStatus = (postID) => {
  FB.api(postID, 'delete', (response) => {
    if (response.error) {
      alert('ERROR: Post not made by this app. Can\'t delete Status');
    } else {
      $("#post_" + postID).remove();
    }
  });
  event.preventDefault();
}