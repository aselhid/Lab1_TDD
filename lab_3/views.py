# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Diary
from datetime import datetime
from django.shortcuts import render, redirect
from django.utils.html import strip_tags
import pytz

# Create your views here.
diary_dict = {}
def index(request):
    diary_dict = Diary.objects.all().values()
    return render(request, 'to_do_list.html', {'diary_dict' : convert_queryset_into_json(diary_dict)})


def add_activity(request):
    if request.method == 'POST':
        activity = strip_tags(request.POST['activity'])
        date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
        Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=activity)
        return redirect('/lab-3/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val
